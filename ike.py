#
# Copyright (c) 2003-2019 Igor Khomyakov. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1) Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2) Redistributions of source code with modification must include a notice
# that the code was modified.
# 3) Neither the names of the authors nor the names of their contributors may
# be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
## CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import sys
from enum import Enum

class CM(Enum):
   NOOP   = 0 # no op
   RETURN = 1 # return
   PUSHV  = 2 # push value
   PUSHA  = 3 # push pc+2, and jump offs
   SCALL  = 4 # stack call --- apply
   CALL   = 5 # call (handles both primitives and non-primitives)
   STOP   = 6 # stop

class TK(Enum):
   EOI    = 0 # end of input
   SYMBOL = 1 # symbol; max SYMBSZ-1 char long
   QSYMBOL= 2 # 'symbol; max SYMBSZ-1 char long
   SOPEN  = 3 # [ open square bracket
   SCLOSE = 4 # ] close square bracket
   INTEGER= 5 # 31 bit non-negative integer
   APPLY  = 6 # @ apply
   STRING = 7 # string
   WHITE  = 8 # white space -- ignored by compiler
   COMMENT= 9 # comment --- ignored by compiler

class Token:
   def __init__(self, typ, value=None):
      self.type = typ # TK
      self.value = value 

class ER(Enum):
   OK = 0
   FATAL = 1
   LOOKUP = 2
   NOTIMPL = 3
   CALL = 4

class CompileError(Exception):
   def __init__(self, err, *args):
      super().__init__(err, *args)

class ExecError(Exception):
   def __init__(self, err, *args):
      super().__init__(err, *args)

class Engine:
   def __init__(self, inp, debug=False):
      self.reset(inp, debug)

   def logg(self, s):
      sys.stderr.write(s)

   def define(self, val, name):
      self.dc.append((name,val))

   def lookup(self, name):
      for i in range(len(self.dc)-1, -1, -1):
         if self.dc[i][0] == name:
            return i
      return -1
      
   def f_if(self):
      "(cf ct bc -- ((bc?ct:cf)@))"
      bc = self.ds.pop()
      ct = self.ds.pop()
      cf = self.ds.pop()
      self.call(ct if bc else cf)

   def f_cond(self):
      "(cf ct bc -- (bc?ct:cf))"
      bc = self.ds.pop()
      ct = self.ds.pop()
      cf = self.ds.pop()
      self.ds.append(ct if bc else cf)

   def f_deci(self):
      "(ix -- (ix-1))"
      self.ds[-1] -= 1

   def f_inci(self):
      "(ix -- (ix+1))"
      self.ds[-1] += 1

   def f_addi(self):
      "(iy ix -- (ix + iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a + b)

   def f_subi(self):
      "(iy ix -- (ix - iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a - b)

   def f_muli(self):
      "(iy ix -- (ix * iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a * b)

   def f_divi(self):
      "(iy ix -- (ix / iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a / b)

   def f_umii(self):
      "(ix -- (- ix))"
      self.ds[-1] *= -1

   def f_eqi(self):
      "(iy ix -- (ix == iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a == b)

   def f_gti(self):
      "(iy ix -- (ix > iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a > b)

   def f_andi(self):
      "(iy ix -- (ix & iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a & b)

   def f_ori(self):
      "(iy ix -- (ix | iy))"
      a = self.ds.pop()
      b = self.ds.pop()
      self.ds.append(a | b)

   def f_negi(self):
      "(ix -- (~ ix))"
      self.ds[-1] = ~self.ds[-1]

   def f_apush(self):
      "(x -- )"
      self.auxs.append(self.ds.pop())

   def f_apop(self):
      "( -- x)"
      self.ds.append(self.auxs.pop())

   def f_dup(self):
      "(x -- x x)"
      self.ds.append(self.ds[-1])

   def f_dup2(self):
      "(y x -- y x y x)"
      self.ds.append(self.ds[-2])
      self.ds.append(self.ds[-2])

   def f_drop(self):
      "(x -- )"
      self.ds.pop()

   def f_drop2(self):
      "(y x -- )"
      self.ds.pop()
      self.ds.pop()

   def f_swap(self):
      "(y x -- x y)"
      self.ds[-1], self.ds[-2] = self.ds[-2], self.ds[-1]

   def f_swap2(self):
      "(y1 y2 x1 x2 -- x1 x2 y1 y2)"
      self.ds[-1], self.ds[-2], self.ds[-3], self.ds[-4] = self.ds[-3], self.ds[-4], self.ds[-1], self.ds[-2]

   def f_rot(self):
      "(z y x -- y x z)"
      self.ds[-1], self.ds[-2], self.ds[-3] = self.ds[-3], self.ds[-1], self.ds[-2]

   def f_rotneg(self):
      "(z y x -- x z y)"
      self.ds[-1], self.ds[-2], self.ds[-3] = self.ds[-2], self.ds[-3], self.ds[-1]

   def f_over(self):
      "(y x -- y x y)"
      self.ds.append(self.ds[-2])

   def f_over2(self):
      "(y1 y2 x1 x2 -- y1 y2 x1 x2 y1 y2)"
      self.ds.append(self.ds[-4])
      self.ds.append(self.ds[-4])

   def f_pick(self):
      "( ix -- (ix-th element))"
      ix = self.ds.pop()
      self.ds.append(self.ds[-ix-1])

   def f_printi(self):
      "( ix -- )"
      print(f"o> {self.ds.pop()}")

   def f_prints(self):
      "( sx -- )"
      print(f"o> {self.ds.pop()}")

   def f_define(self):
      "( code name -- )"
      name = self.ds.pop()
      code = self.ds.pop()
      self.define(code, name)

   def f_redefine(self):
      "( code name -- )"
      name = self.ds.pop()
      code = self.ds.pop()
      i = self.lookup(name)
      if i == -1:
         self.define(code, name)
      else:
         self.dc[i][1] = code

   def f_lookup(self):
      "( name -- code )"
      name = self.ds.pop()
      i = self.lookup(name)
      if i == -1:
         raise ExecError(ER.LOOKUP, name)
      self.ds.append(self.dc[i][1])


   def reset(self, inp, debug=False):
      self.dc = [] # dictionary
      self.code = [CM.STOP]
      self.pc = 1  # program counter: (code, offset)
      self.rs = [0] # return stack: list of pc
      self.ds = [] # data stack
      self.auxs = [] # auxiliary data stack

      self.inp = iter(inp) # inp is iterable char
      self.uninput = None # lexer 1 char "unget" buffer */

      self.err = ER.OK ## ErrorCode
      self.debug = debug # debug flag
      self.count = 0 # int

      self.define(self.f_define,"def");
      self.define(self.f_redefine,"redefine");
      self.define(self.f_lookup,"lookup");
      self.define(self.f_inci,"inc");
      self.define(self.f_deci,"dec");
      self.define(self.f_addi,"+");
      self.define(self.f_subi,"-");
      self.define(self.f_muli,"*");
      self.define(self.f_divi,"/");
      self.define(self.f_umii,"neg");
      self.define(self.f_eqi,"=");
      self.define(self.f_gti,">");
      self.define(self.f_andi,"&");
      self.define(self.f_ori,"|");
      self.define(self.f_negi,"~");
      self.define(self.f_dup,"dup");
      self.define(self.f_dup2,"dup2");
      self.define(self.f_drop,"drop");
      self.define(self.f_drop2,"drop2");
      self.define(self.f_swap,"swap");
      self.define(self.f_swap2,"swap2");
      self.define(self.f_rot,"rot");
      self.define(self.f_rotneg,"-rot");
      self.define(self.f_over,"over");
      self.define(self.f_over2,"over2");
      self.define(self.f_pick,"pick");
      self.define(self.f_apush,">a");
      self.define(self.f_apop,"a>");
      self.define(self.f_printi,".");
      self.define(self.f_prints,"s.");
      self.define(self.f_cond,"cond");
      self.define(self.f_if,"if");

   def input(self):
      return next(self.inp, None)

   def lexer(self):
      qflag = False;
      if (self.uninput is not None):
         c = self.uninput
         self.uninput = None
      else:
         c = self.input()

      while c in [' ','\t','\n','\r']:
         c = self.input()

      if c is None:
         return Token(TK.EOI)
      elif c == '[':
         return Token(TK.SOPEN)
      elif c == ']':
         return Token(TK.SCLOSE)
      elif c == '@':
         return Token(TK.APPLY)
      elif c == '"':
         c = self.input()
         eflag = False
         i = 0
         s = ''
         while (c != '"' or eflag) and c!=None:
            if c == '\r': # dos \r filtered out
               continue
            if not eflag and c == '\\':
               eflag=True
            else:
               if eflag:
                  if c == '\n': # eol \ supresses \n
                     continue
                  eflag = False;
                  if c == 't':
                     c = '\t'
                  elif c == 'r':
                     c = '\r'
                  elif c == 'n':
                     c = '\n'
                  elif c == 'f':
                     c = '\f'
               s += c
            c = self.input()
         if c != '"':
            self.uninput = c;
         return Token(TK.STRING, s)

      if c >= '0' and c <= '9':
         n = ord(c) - ord('0')
         c = self.input()
         while c >= '0' and c <= '9':
            n = n * 10 + (ord(c) - ord('0'))
            c = self.input()
         self.uninput = c;
         return Token(TK.INTEGER, n);
   
      i = 0
      s = ''
      while c not in [None, ' ', '\t', '\n', '\r', '[', ']', '"']:
         if i == 0 and c == "'":
            qflag = True;
            i -= 1
         else:
            s += c
         i += 1
         c = self.input()

      if not qflag and s[0] == '-' and s[1]=='-': # comment
         while c not in [None, '\n']:
            c = self.input()
         self.uninput = c;
         return Token(TK.COMMENT, s);
      else:
         self.uninput = c
         return Token(TK.QSYMBOL if qflag else TK.SYMBOL, s)
   

   def compile(self):
      if self.debug:
         self.logg(f"\n")
      cs = [] # compile stack to compile arrays
      t = self.lexer()
      while t.type != TK.EOI:

         if t.type == TK.INTEGER:
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.PUSHV.name} integer = <{t.value}>\n")
            self.code.extend([CM.PUSHV, t.value])

         elif t.type == TK.QSYMBOL:
            i = self.lookup(t.value)
            if i == -1:
               raise CompileError(ER.LOOKUP, t.value)
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.PUSHV.name} qsymbol({t.value}) = <{self.dc[i][1]}>\n")
            self.code.extend([CM.PUSHV, self.dc[i][1] ])

         elif t.type == TK.SYMBOL:
            i = self.lookup(t.value)
            if i == -1:
               raise CompileError(ER.LOOKUP, t.value)
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.CALL.name} symbol({t.value}) = <{self.dc[i][1]}>\n")
            self.code.extend([CM.CALL, self.dc[i][1] ])

         elif t.type == TK.APPLY:
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.SCALL.name}\n")
            self.code.append(CM.SCALL)

         elif t.type == TK.STRING:
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.PUSHV.name} string = <{t.value}>\n")
            self.code.extend([CM.PUSHV, t.value ])

         elif t.type == TK.SOPEN:
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.PUSHA.name}\n")
            self.code.append(CM.PUSHA)
            cs.append( len(self.code) )
            if self.debug:
               self.logg(f"c> {len(self.code)}: ???\n")
            self.code.append(None)

         elif t.type == TK.SCLOSE:
            if self.debug:
               self.logg(f"c> {len(self.code)}: {CM.RETURN.name}\n")
            self.code.append(CM.RETURN)
            if self.debug:
               self.logg(f"c>   {len(self.code)} -> @{cs[-1]}\n", )
            self.code[ cs.pop() ] = len(self.code)

         #elif t in [TK.WHITE, TK_COMMENT]:
         #   pass

         t = self.lexer()

      if self.debug:
         self.logg(f"c> {len(self.code)}: {CM.RETURN.name}\n\n")
      self.code.append(CM.RETURN)

   def call(self, code):
      if callable(code):
         code()
      elif isinstance(code, int):
         if self.code[self.pc] != CM.RETURN: # tro
            self.rs.append(self.pc)
         self.pc = code
      else:
         ExecError(ER.CALL)

      
   
   def execute(self):
      while True:
         self.count += 1
         cmd = self.code[ self.pc ]
         if self.debug:
            self.logg(f"e> {self.pc}: {cmd.name}");
         self.pc += 1

         if cmd == CM.NOOP: # do nothing
            pass

         elif cmd == CM.PUSHV:
            if self.debug:
               self.logg(f" value={self.code[ self.pc ]}");
            self.ds.append( self.code[ self.pc ] )
            self.pc += 1

         elif cmd == CM.PUSHA:
            if self.debug:
               self.logg(f" value={self.pc + 1} next={self.code[ self.pc ]}");
            self.ds.append(self.pc + 1)
            self.pc = self.code[self.pc]

         elif cmd == CM.CALL:
            if self.debug:
               self.logg(f" code={self.code[ self.pc ]}");
            code = self.code[ self.pc ]
            self.pc += 1
            self.call(code)

         elif cmd == CM.SCALL:
            if self.debug:
               self.logg(f" code={self.ds[-1]}");
            code = self.ds.pop()
            self.call(code)

         elif cmd == CM.RETURN:
            self.pc = self.rs.pop()

         elif cmd == CM.STOP:
            if self.debug:
               self.logg(f" ds={self.ds}, rs={self.rs}, auxs={self.auxs}\n\n");
            return

         else:
            raise ExecError(ER.NOTIMPL)
         
         if self.debug:
            self.logg(f" ds={self.ds}, rs={self.rs}, auxs={self.auxs}\n");
   

   def print(self):
      self.logg(f"Dictionary: {self.dc}\n") 
      self.logg(f"Return stack: {self.rs}\n") 
      self.logg(f"Data stack: {self.ds}\n") 
      self.logg(f"Aux data stack: {self.auxs}\n") 
      self.logg(f"Code: code={self.code}\n") 
      self.logg(f"Registers: Status={self.err}, pc={self.pc}, count={self.count}, debug={self.debug}\n") 
   
   def usage(self):
      self.logg("***\n");
      slef.logg("*** IKE a.k.a COOL a.k.a AE a.k.a. ASBF a.k.a. DOG a.k.a. C8L\n");
      slef.logg("*** Inspired by JOY\n");
      slef.logg("*** A Stack Based Functional Programming Language\n");
      slef.logg("***\n");
      slef.logg("*** Copyright (c) 2003-2019 Igor Khomyakov\n");
      slef.logg("***\n");
      slef.logg("usage: python ike[d].py { file | - | =code } ...\n");
      slef.logg("tokens: [ ] @ name 'name \"string\" integer --comment\n");
      slef.logg(f"dictionary: {self.dc}\n");
      slef.logg("\n");

if __name__ == '__main__':
   s = sys.stdin.read()
   e = Engine(s, debug=True)
   try:
      e.compile()
      e.execute()
   finally:
      e.print()
